﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MariaBaque3.B
{
    class Empleados : Persona
    {
        public DateTime FechaIngreso { get; set; }
        public int NumDespacho { get; set; }

        public Empleados(DateTime Fechaingreso, int NumDespacho, string Nombre, string Apellido, string Cedula, string EstadoCivil) : base(Nombre, Apellido, Cedula, EstadoCivil)
        {
            this.NumDespacho = NumDespacho;
            this.FechaIngreso = FechaIngreso;
        }
        public void reubicaciondespacho(int NumDespacho)
        {
            this.NumDespacho = NumDespacho;

        }
    }
}