﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MariaBaque3.B
{
    class EmpleadoPorHoras
    {
        static private float ValorDeHora = 2.50f;
        private int horasTrabajadas;


        public EmpleadoPorHoras(int horasTrabajadas, string apellidos, string nombres, int Edad, string Departamento)
        {
            this.horasTrabajadas = horasTrabajadas;
        }

        public int getHorasTrabajadas()
        {
            return horasTrabajadas;
        }

        public void setHorasTrabajadas(int horasTrabajadas)
        {
            this.horasTrabajadas = horasTrabajadas;
        }

        //METODO----------

        public float CalcularSueldo()
        {
            float sueldo = horasTrabajadas * ValorDeHora;
            return sueldo;
        }
    }
}
