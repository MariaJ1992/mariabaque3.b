﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MariaBaque3.B
{
    class EmpleadoTemporal : Empleado
    {
        private int Id;
        private DateTime FechaDeIngreso;
        private DateTime FechaDeSalida;
        static private int sueldo = 399;

        public EmpleadoTemporal(DateTime FechaDeIngreso, DateTime FechaDeSalida, string apellidos, string nombres, int Edad, string Departamento) : base(apellidos, nombres, Edad, Departamento)
        {
            this.FechaDeIngreso = FechaDeIngreso;
            this.FechaDeSalida = FechaDeSalida;
        }

        public int setId()
        {
            return Id;
        }
        public void getId(int Id)
        {
            this.Id = Id;
        }

        public DateTime getFechaDeIngreso()
        {
            return FechaDeIngreso;
        }

        public void setFechaDeIngreso(DateTime FechaDeIngreso)
        {
            this.FechaDeIngreso = FechaDeIngreso;
        }
    }
}