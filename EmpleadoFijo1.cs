﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MariaBaque3.B
{
    class EmpleadoFijo : Empleado
    {
        private int mesesTrabajados;
        static private float sueldoMensual = 450;
        public EmpleadoFijo(int mesesTrabajados, string apellidos, string nombres, int Edad, string Departamento) : base(apellidos, nombres, Edad, Departamento)
        {
            this.mesesTrabajados = mesesTrabajados;
        }
        //getters and setters
        public int getFechaDeEntrada()
        {
            return mesesTrabajados;
        }
        public void setFechaDeEntrada(int FechaDeEntrada)
        {
            this.mesesTrabajados = mesesTrabajados;
        }


        //METODO------------------
        public float CalcularSueldo()
        {
            float sueldo = mesesTrabajados * sueldoMensual;

            return sueldo;
        }
    }
}
