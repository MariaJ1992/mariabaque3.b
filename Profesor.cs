﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MariaBaque3.B
{
    class Profesor : Empleados
    {
        public string Departamento { get; set; }

        public Profesor(string Departamento, DateTime FechaIngreso, int NumDespacho, string Nombre, string Apellido, string Cedula, string EstadoCivil) : base(FechaIngreso, NumDespacho, Nombre, Apellido, Cedula, EstadoCivil)
        {
            this.Departamento = Departamento;
        }
        public void Cambiodedepartamento(string departamento)
        {
            Departamento = departamento;
        }
        public void Mostrardatos()
        {
            Console.WriteLine("Datos empleados:\nNombres y Apellidos: " + Nombre + " " + Apellido + "\n" +
            "Num cedula: " + Cedula + "\n" +
            "Estado Civil: " + EstadoCivil + "\n" +
            "Fecha de ingreso: " + FechaIngreso + "\n" +
            "Departamento: " + Departamento);
        }
    }
}