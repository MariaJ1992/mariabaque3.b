﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MariaBaque3.B
{
    class TADPila
    {
        public static int tamano = 4;
        public static int[] pila = new int[tamano];
        public static int cima = -1;

        public Boolean Vacia()
        {
            return (pila.Length - 1) == cima;
        }
        public void Apilar(int dato)
        {
            if (cima < tamano)
            {
                cima++;
                pila[cima] = dato;
            }

        }
        public int Desapilar()
        {
            int valor = Top();
            cima--;
            return valor;
        }
        public int Top()
        {
            return pila[cima];
        }
    }
}