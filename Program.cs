﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MariaBaque3.B
{
    class Program
    {
        static void Main(string[] args)
        {
            string datosEmpleado = "";

            EmpleadoFijo empleadoFijo = new EmpleadoFijo(12, "Velez", "Jeniffer", 36, "Limpieza");
            datosEmpleado = "Nombre: " + empleadoFijo.getNombres() + "\nApellidos: " + empleadoFijo.getApellidos() + "\nSueldo:" + empleadoFijo.CalcularSueldo();
            Console.WriteLine(datosEmpleado);
            Console.WriteLine("\n***************\n");

            EmpleadoTemporal empleadoTemporal = new EmpleadoTemporal(new DateTime(2004, 7, 2, 5, 33, 57), new DateTime(2008, 7, 2, 5, 33, 57), "Velez", "Jeniffer", 36, "Limpieza");
            datosEmpleado = "Nombre: " + empleadoTemporal.getNombres() + "\nApellidos: " + empleadoTemporal.getApellidos() + "\nSueldo:" + empleadoFijo.CalcularSueldo();
            Console.WriteLine(datosEmpleado);
            Console.WriteLine("\n***************\n");

            Estudiante estudiante = new Estudiante(1, "Jeniffer", "Velez", "1313154904", "Soltera");
            Profesor profesor = new Profesor("Arquitectura", 2, new DateTime(2004, 7, 2, 5, 57), "Jeniffer", "Velez", "1313154904", "Soltera");
            PersonalServicio personaDeServicio = new PersonalServicio("secretaría", 2, new DateTime(2004, 7, 2, 5, 57), "Jeniffer", "Velez", "1313154904", "Soltera");
            estudiante.Mostrardatos();
            profesor.Mostrardatos();
            personaDeServicio.Mostrardatos();
        }
    }
}
