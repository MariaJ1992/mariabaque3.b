﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MariaBaque3.B
{
    class Empleado
    {
        private int Id;
        private string Apellidos;
        private string Nombres;
        private int Edad;
        private string Departamento;

        public Empleado(string apellidos, string nombres, int Edad, string Departamento)
        {
            Apellidos = apellidos;
            this.Nombres = nombres;
            this.Edad = Edad;
            this.Departamento = Departamento;
        }

        public int getId()
        {
            return Id;
        }
        public void setId(int Id)
        {
            this.Id = Id;
        }
        public string getApellidos()
        {
            return Apellidos;
        }
        public void setApellidos(string Apellidos)
        {
            this.Apellidos = Apellidos;
        }

        public string getNombres()
        {
            return Nombres;
        }
        public void setNombres(string Nombres)
        {
            this.Nombres = Nombres;
        }

        public int getEdad()
        {
            return Edad;
        }

        public void setEdad(int Edad)
        {
            this.Edad = Edad;
        }


        public string getDepartamento()
        {
            return Departamento;
        }

        public void setDepartamento(string Departamento)
        {
            this.Departamento = Departamento;
        }
    }
}
