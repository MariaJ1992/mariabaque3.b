﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MariaBaque3.B
{
    class PersonalServicio : Empleados
    {
        public string Seccion { get; set; }

        public PersonalServicio(string Seccion, DateTime FechaIngreso, int NumDespacho, string Nombre, string Apellido, string Cedula, string EstadoCivil) : base(FechaIngreso, NumDespacho, Nombre, Apellido, Cedula, EstadoCivil)
        {
            this.Seccion = Seccion;
        }
        public void trasladodeseccion(string seccion)
        {
            this.Seccion = seccion;
        }
        public void Mostrardatos()
        {
            Console.WriteLine("Datos empleados:\nNombres y Apellidos: " + Nombre + " " + Apellido + "\n" +
            "Num cedula: " + Cedula + "\n" +
            "Estado Civil: " + EstadoCivil + "\n" +
            "Fecha de ingreso: " + FechaIngreso + "\n" +
             "Sección: " + Seccion);
        }
    }
}
