﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MariaBaque3.B
{
    class Estudiante : Persona
    {
        public int Curso;
        public int curso { get; set; }
        public Estudiante(int Curso, string Nombre, string Apellido, string Cedula, string EstadoCivil) : base(Nombre, Apellido, Cedula, EstadoCivil)
        {
            this.Curso = Curso;
        }
        public void matriculacion(int Curso)
        {
            this.Curso = Curso;
        }
        public void Mostrardatos()
        {
            Console.WriteLine("Datos empleados:\nNombres y Apellidoa: " + Nombre + " " + Apellido + "\n" +
            "Num cedula: " + Cedula + "\n" +
            "Estado Civil: " + EstadoCivil + "\n" +
            "Curso: " + Curso + "\n");
        }
    }
}
