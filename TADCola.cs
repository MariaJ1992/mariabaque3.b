﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MariaBaque3.B
{
    class TADCola
    {
        public static int tamano = 6;
        public int[] cola = new int[tamano];
        public int primero = -1;
        public int ultimo = -1;

        public Boolean Vacia()
        {
            return (primero == -1 & ultimo == -1);
        }
        public void Encolar(int dato)
        {
            if (Vacia())
            {
                primero++;
                ultimo++;
                cola[ultimo] = dato;
            }
        }
        public int Desencolar()
        {
            int valor = cola[primero];
            primero++;
            if (primero == tamano)
            {
                primero = 0;
            }

            return valor;
        }
        public int Ultimo()
        {
            return cola[ultimo];
        }
    }
}
